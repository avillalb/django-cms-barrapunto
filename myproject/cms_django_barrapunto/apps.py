from django.apps import AppConfig


class CmsDjangoBarrapuntoConfig(AppConfig):
    name = 'cms_django_barrapunto'
