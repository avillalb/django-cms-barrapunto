from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, HttpResponseNotFound
from .models import WebPage
from django.views.decorators.csrf import csrf_exempt
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from urllib import request

# Create your views here.

global lista
formulario = """
    <form method="post" action="">
        \n\n<br>
        <meta charset=UTF-8> 
        <textarea name="resource_content" rows="10" cols="40">{default_value}</textarea>
        <input type="submit" value="Enviar formulario">
    </form>
    """
body = ""


def xml_parser(url):
    html_template = """<!DOCTYPE html>
    <html lang="es" >
      <head>
        <meta charset="utf-8" />
      </head>
      <body>
        {body}
      </body>
    </html>
    """
    html_item_template = "<li><a href='{link}'>{title}</a></li>"

    class CounterHandler(ContentHandler):

        def __init__(self):
            self.inItem = 0
            self.inContent = False
            self.theContent = ""
            self.title = ""
            self.link = ""

        def startElement(self, name, attrs):
            if name == 'item':
                self.inItem = 1
            elif name == 'title':
                if self.inItem:
                    self.inContent = True
                    self.theContent = ""
            elif name == 'link':
                if self.inItem:
                    self.inContent = True
                    self.theContent = ""

        def endElement(self, name):
            global body

            if name == 'item':
                self.inItem = 0
                body += html_item_template.format(link=self.link, title=self.title)

            elif name == 'title' and self.inContent:
                self.title = self.theContent
                self.inContent = False
            elif name == 'link' and self.inContent:
                self.link = self.theContent
                self.inContent = False

            if self.inContent:
                self.inItem = 0
                self.theContent = ""

        def characters(self, chars):
            if self.inContent:
                self.theContent = self.theContent + chars

    MyParser = make_parser()
    MyHandler = CounterHandler()
    MyParser.setContentHandler(MyHandler)

    xmlFile = request.urlopen(url)

    MyParser.parse(xmlFile)
    lista_parseada = html_template.format(body=body)
    return (lista_parseada)


lista = xml_parser("http://barrapunto.com/index.rss")


@csrf_exempt
def home(request):
    if request.method == "GET":
        reply = "lista de paginas: <br>"
        pages = WebPage.objects.all()
        if pages.count() != 0:
            for p in pages:
                reply += p.name + " -> " + p.content + "</br>"
        else:
            reply += "<h4>No hay paginas almacenadas </h4></br>"

        return HttpResponse(reply)


@csrf_exempt
def process(request, resource):
    if request.method == "GET":  # muestra formulario con el valor por defecto si lo tiene, si no se muestrara en blanco
        try:
            p = WebPage.objects.get(name=resource)
            content = p.content
            reply = formulario.format(default_value=content)
        except WebPage.DoesNotExist:
            reply = formulario.format(default_value="")

        return HttpResponse(reply + lista)


    elif request.method == "POST":  # recibe el formulario relleno

        new_content = request.POST['resource_content']

        if len(new_content) == 0:  # formulario va vacio, lo muestro vacio pero NO borro el contenido anterior
            reply = formulario.format(default_value="")
        else:  # formulario tiene contenido
            try:
                page = WebPage.objects.get(name=resource)
                page.content = new_content
                page.save()
            except WebPage.DoesNotExist:
                # añado nueva entrada
                nueva_page = WebPage(name=resource, content=new_content)
                nueva_page.save()

            reply = formulario.format(default_value=new_content)

        return HttpResponse(reply + lista)
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


